## Installation

### PHP

Install new package

```
docker run --rm -it -v $(pwd):/app composer require <package>
```

Compose install

```
docker run --rm -it -v $(pwd):/app composer install    
```

### Docker

Run docker

`docker-compose up -d`

Destroy docker

`docker-compose down`

