<?php
require_once(__DIR__ . '/../vendor/autoload.php');
(\Dotenv\Dotenv::create(__DIR__ . '/../'))->load();
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', getenv('DB_NAME') );

/** MySQL database username */
define( 'DB_USER', getenv('DB_USER') );

/** MySQL database password */
define( 'DB_PASSWORD', getenv('DB_PASSWORD') );

/** MySQL hostname */
define( 'DB_HOST', getenv('DB_HOST') );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', getenv('DB_CHARSET') );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '~rK.O0bD.42Bi9+K2&OYf)_(:h>y9)TD|l8GI lrX&bT}UPTeMA?h(F`_v=e-EE~' );
define( 'SECURE_AUTH_KEY',  '}k{W@UEO!:%[*9RohaKV0L# u(xY%:?*qRZ4# qm:Yyr!|K9&k(r&}hq~3iSIm#9' );
define( 'LOGGED_IN_KEY',    'Zgwr9sajL}x(tMHeP3EFG?siimG1W7-<6k&:u]-{;)eyF]L$Nm*6j3hw%[E/~1l3' );
define( 'NONCE_KEY',        'pgjhy>Ou?JiQF $u&b?t+ s:827^bW*l:>8O?zEvt9S{~~$;aA+_C+QyNs1%4n1O' );
define( 'AUTH_SALT',        'iS>2l?uA(S68;S.L9E4TM{/9|7?>-;oq.DQYL}no*>s)qX`uX${1.<tFKC-7,c@I' );
define( 'SECURE_AUTH_SALT', 'pJ8?X<5j r)gzb~0^WET}ig-XCDTp,`,$|A7U =Y =r tFwu5{(10AX(8;FCtLyp' );
define( 'LOGGED_IN_SALT',   ',K12$M3jXv6W~dWd_Ud>O?PcBM/l.&2B[b3|bs]wJ.:xhH)AxNTv|`pO!~|LA3af' );
define( 'NONCE_SALT',       ']R??p0O%D*0rEh2*y&?N 1S@SPEH=0+?AjWu9,(%!|SVU;_;J#U,211uh)+:8S^:' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environmenfalsets.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', getenv('WP_DEBUG'));

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
